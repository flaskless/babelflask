# Babel-Flask


Simple online PO/POT translation file editor. 

> ... I think it's better way than sending the .po file back and forth via email when it need some changes.

Built on Flask and Python PoLib.

## What does it do?


Upload a .po or .pot file to this app, copy the edit link, and send the link to a partner to do the translation.

Your partner can use use the web based interface to translate, edit, save and download the result as `.po` format or the compiled version (`.mo`) file.

## Screenshots

Upload a `.po` file with the following contents:

```po
msgid ""
msgstr ""
"Project-Id-Version: Lingohub 1.0.1\n"
"Report-Msgid-Bugs-To: support@lingohub.com \n"
"Last-Translator: Marko Bošković <marko@lingohub.com>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Let’s make the web multilingual."
msgstr "Machen wir das Internet mehrsprachig."

msgid "We connect developers and translators around the globe "
"on Lingohub for a fantastic localization experience."
msgstr "Wir verbinden Entwickler mit Übersetzern weltweit "
"auf Lingohub für ein fantastisches Lokalisierungs-Erlebnis."

msgid "Welcome back, %1$s! Your last visit was on %2$s"
msgstr "Willkommen zurück, %1$s! Dein letzter Besuch war am %2$s"

msgid "%d page read."
msgid_plural "%d pages read."
msgstr[0] "Eine Seite gelesen wurde."
msgstr[1] "%d Seiten gelesen wurden."
```

![upload](docs/poredit-result.png)

![upload](docs/poredit-upload.png)


# How does it work?

This app built entirely on top of Open Source software such as:

- **Flask**, a micro web framework built on Python. It's just perfect for this kind of task and utility. Although it can be used to create large a scale web apps too.
- **PoLib**, a Python module that can be used to create, read and modify PO and MO translation file.
- **Bootstrap** from Twitter, just an amazing UI framework that can let me focus on the backend code, and let Bootstrap handle the beautiful think at the front.
