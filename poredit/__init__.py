import os
from flask import Flask, request, redirect, render_template
from flask import url_for, flash, current_app, redirect
from werkzeug.utils import secure_filename

from . import utils
from . import settings

PO_FILE = 'PO_FILE'

po_file = os.getenv(PO_FILE)
upload_dir = os.getcwd()
filename = None

if po_file:
    upload_dir = os.path.dirname(po_file)
    filename = os.path.basename(po_file)

app = Flask(__name__)
app.config.from_object(settings)
app.config['UPLOAD_FOLDER'] = upload_dir
app.config[PO_FILE] = filename

@app.route('/', methods=['GET', 'POST'])
def home():
    """
    Our homepage request handler
    """
    
    filename =  current_app.config.get(PO_FILE)
    if filename:
        return redirect(url_for('.translate',filename=filename))
    context = {
        'title': 'An online PO file editor and compiler',
        'page': 'home'
    }
    
    if request.method == 'GET':
        return render_template('home.html', **context)
    else:
        # If POST request
        file = request.files['pofile']
        if file and utils.allowed_file(file.filename, app.config['ALLOWED_EXTENSIONS']):

            filename = secure_filename(file.filename)
            file_ext = utils.get_file_ext(filename)
            
            po_id = utils.generate_random_numbers(20)            
            filename = '%s.%s' % (po_id, file_ext)

            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('translate', filename=filename))
        else:
            flash('Invalid file extension. We only accept (.po, .pot) file!', 'error')
            return redirect(url_for('home'))
            

@app.route('/translate/<filename>/', methods=['GET', 'POST'])
def translate(filename):
    """
    File editing handler
    """
    
    if request.method == 'POST':
        return utils.save_translation(app, request, filename)
    else:
        return utils.open_editor_form(app, request, filename)
    

@app.route('/download/<filename>/', methods=['GET'])
def download(filename):
    """ File download handler """
    
    ftype = request.args.get('type')
    if ftype and ftype == 'mo':
        return utils.download_mo(app, request, filename)
        
    return utils.download_po(app, request, filename)
    
    
@app.route('/about/', methods=['GET'])
def about():
    context = {
        'title': 'About',
        'page': 'about'
    }
    return render_template('about.html', **context)
    

if __name__ == '__main__':
    app.run()